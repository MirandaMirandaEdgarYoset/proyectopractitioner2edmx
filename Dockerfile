#Imagen base
FROM node:12.18.0

#Directorio de la app en el contenedor
WORKDIR /app

#Copiado de archivos
ADD build/es6-bundled /app/build/es6-bundled
ADD server.js /app
ADD package.json /app

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3000

#Comandos de ejecución de la aplicación
CMD ["npm", "start"]
