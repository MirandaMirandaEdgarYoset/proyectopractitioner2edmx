/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import './my-icons.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class MyAppNav extends PolymerElement {
  static get template() {
    return html`
       

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>
 
          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
            
          <app-login name="login"></app-login>
          <app-dashboard name="dashboard"></app-dashboard>
          <app-register name="register"></app-register>  
          <app-profile name="profile"></app-profile> 
          <app-movements name="movements"></app-movements>                                   
            <app-atms name="atms"></app-atms>
              <app-error404 name="view404"></app-error404>
                  
          </iron-pages>
        
      
         
        
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object
    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
     // Show the corresponding page according to the route.
     //
     // If no page was found in the route data, page will be an empty string.
     // Show 'view1' in that case. And if the page doesn't exist, show 'view404'.
     if(!page) {
      this.page = 'login';
    } 
    else if (['login','myapp','dashboard','atms','register','profile','movements'].indexOf(page) !== -1) {
      this.page = page;
    }
    else {
      this.page = 'view404';
    }
   
  }

  _pageChanged(page) {
    // Import the page component on demand.
    //
    // Note: `polymer build` doesn't like string concatenation in the import
    // statement, so break it up.
    switch (page) {
      case 'login':
        import('./pages/app-login.js');
        break;
      case 'register':
        import('./pages/app-register.js');
        break;  
      case 'profile':
        import('./pages/app-profile.js');
        break;     
      case 'myapp':
        import('./pages/my-app.js');
        break;
      case 'dashboard':
        import('./pages/app-dashboard.js');
        break;
      case 'atms':
        import('./pages/app-atms.js');
      break;
      case 'movements':
        import('./pages/app-movements.js');
      break;       
      case 'view404':
        import('./pages/app-error404');
        break;
    }
  }
}

window.customElements.define('my-appnav', MyAppNav);
