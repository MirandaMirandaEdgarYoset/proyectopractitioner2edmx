import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-card/paper-card.js';
import {GoogleCharts} from 'google-charts';

class PanelChart extends PolymerElement {
  static get template() {
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <style> 
        #chart_wrap {
            position: relative;
            padding-bottom: 100%;
            height: 0;
            overflow:hidden;
        }

        #chart1 {
            position: relative;
            width:100%;
            height:200%;
            padding-bottom: 50%;
        } 
      </style>
      <div id="chart1"></div>
     <iron-ajax
     id=getDataBanxico     
     method="GET"
     url=[[url]]
     data-type="jsonp"
     on-response="getData"
     params='{"token":"eca8574de4f32ff72cde3b415124ec0dbe85dc58637c134a13d15dbdc38ef7b1"}'
     debounce-duration="30">
    </iron-ajax>
    `;
  }

  static get properties(){
    return{
      url:{
        type:String
      }
      ,
        chartData: {
            type:Array,
            value:[['Fecha', 'Dólar E.UA', 'Euro', 'Dólar Canadiense'],["",0,0,0]]
        },
        series:{
          type:Object,
          value:{"SF43718":"Dolar E.U.A","SF46410":"Euro","SF60632":"Dólar Canadiense"}
        },
        meses:{
          type:Array,
          value:["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
        }

    }
  }

  ready() {
    super.ready();
    this.getCurrentDate();
    this.$.getDataBanxico.generateRequest();
  }

  getData(e,request){
      let dates=[];      
      let data=request.response;
      dates.push(['Fecha']);
      data.bmx.series[0].datos.forEach(e => {
        dates.push([e.fecha])
      });
      for (let index = 0; index < data.bmx.series.length; index++) {      
        if(data.bmx.series[index].idSerie=='SF43718'){
          dates[0].push('Dólar E.UA');
          let dolarEUA=data.bmx.series[index].datos;
          dates=this.insertData(dates,dolarEUA)
        }
        if(data.bmx.series[index].idSerie=='SF46410'){
          dates[0].push('Euro');
          let euro=data.bmx.series[index].datos;
          dates=this.insertData(dates,euro)
        }
        if(data.bmx.series[index].idSerie=='SF60632'){
          dates[0].push('Dólar Canadiense');
          let dolarCanadiense=data.bmx.series[index].datos;
          dates=this.insertData(dates,dolarCanadiense)
        }
      }      
      this.chartData=dates;
      requestAnimationFrame(() => {
        GoogleCharts.load(() => this.drawChart());
      });
  }

  insertData(dates,money){
    let array=dates;
    for (let index = 0; index < money.length; index++) {
      array[index+1].push(parseFloat(money[index].dato));
    }
    return array;
  }

  drawChart() {
      let data = google.visualization.arrayToDataTable(this.chartData);
      let mes = new Date().getMonth();
      let options = {
        title: 'Historico Tipos de Cambio de '+this.meses[mes],        
        legend: { position: 'bottom' },
        width: '100%',
        height: '500px'
      };
      const pie_1_chart = new google.visualization.LineChart(this.root.getElementById('chart1'));
      pie_1_chart.draw(data,options);
  }
  
  getCurrentDate(){
    let date=new Date();
    let startDate=date.getFullYear()+'-'+(date.getMonth()+1)+'-01';
    let currentDate=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
    this.$.getDataBanxico.url='https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718,SF46410,SF46406,SF60632/datos/'+startDate+'/'+currentDate;
  }
}

window.customElements.define('panel-chart', PanelChart);
