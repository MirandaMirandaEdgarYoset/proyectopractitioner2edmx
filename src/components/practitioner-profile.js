/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import './card-money';

class Profile extends PolymerElement {
    static get properties() {
        return {
            cuentas:{type:Array},
            options:{type:Array},
            transferButton: {type: Boolean, value: false},
            dataCards: {type: Array}
        };
    }

    static get template() {
        return html`         
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">   
        <style>
            .header{
                width: 75%;
                margin-left: auto;
                margin-right: auto;
                text-align: center;
            }
            .contenedor{
                width: 75%;
                margin-left: auto;
                margin-right: auto;
                border-radius: 30px 50px;
            }

            .acciones{
                width: 75%;
                margin-left: auto;
                margin-right: auto;
                display: flex;
                justify-content: space-evenly;
            }

        </style>

        <!--Estilos de la card-->
        <style>
            * {
            box-sizing: border-box;
            }

            body {
            font-family: Arial, Helvetica, sans-serif;
            }

            /* Float four columns side by side */
            .column {
            float: left;
            width: 25%;
            padding: 0 10px;
            }

            /* Remove extra left and right margins, due to padding */
            .row {margin: 0 -5px;}

            /* Clear floats after the columns */
            .row:after {
            content: "";
            display: table;
            clear: both;
            }

            /* Responsive columns */
            @media screen and (max-width: 600px) {
            .column {
                width: 100%;
                display: block;
                margin-bottom: 20px;
            }
            }

            /* Style the counter cards */
            .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            padding: 16px;
            text-align: center;
            background-color: #f1f1f1;
            }
        </style>

            <div class="mi-perfil">
                <div class="header">
                    <h1>Mi perfil</h1>
                </div>
                <hr>
                <div class="acciones">
                    <button class="transferir btn btn-success" on-click="transferButtonActivate" > Transferir</button>
            
                </div>
                <hr>
                <div class="contenedor">
                    <div class="cuentas">
                        <h1>Cuentas</h1>
                        <p>Aquí puedes ver la información de tus cuentas</p>
                        <div class="row">
                            <template is="dom-repeat" items="[[dataCards]]">
                                <p>
                                    <div class="col-md-4">
                                        <card-money tarjeta=[[item.accountNumber]] saldo="[[item.amount.$numberDecimal]]">/<card-money>
                                    </div>
                                </p>
                            </template>
                        </div> 
                    </div>
                    <br><hr>
                    <div class="tarjetas">
                        <h1>Tarjetas</h1>
                        <p>Aquí puedes ver la información de tus tarjetas</p>
                        <div class="row">
                            <template is="dom-repeat" items="[[dataCards]]">
                                <p>
                                    <div class="col-md-4">
                                    <card-money tarjeta=[[item.debitCard.number]] saldo="[[item.amount.$numberDecimal]]">/<card-money>
                                    </div>
                                </p>
                            </template>
                        </div> 
                    </div>
                    </div>
                </div>
            </div>

            <iron-ajax
                id="getAccounts"
                content-type="application/json"
                crossDomain: true
                method="GET",
                handle-as="json"
                on-response="manageAJAXResponse"
                on-error="showError">
            </iron-ajax>

            <iron-ajax
                id="putTransfer"
                content-type="application/json"
                crossDomain: true
                method="PUT",
                handle-as="json"
                on-response="manageAJAXTransferResponse"
                on-error="showTransferError">
            </iron-ajax>

        `;
    }

    ready() {
        super.ready();
        this.transferButton = false;
        this.getAccounts();
    }


    transferButtonActivate() {
        this.transferButton = true;
        this.getAccounts();
    }

    getAccounts(){
        let myemail = sessionStorage.getItem('Email');
        let mytoken = sessionStorage.getItem('Token');
        this.$.getAccounts.url=baseApi+'accounts';
        this.$.getAccounts.headers = {
            "Access-Control-Allow-Origin":"*",
            "Authorization": 'Bearer ' + mytoken,
            "customer": myemail
        }
        this.$.getAccounts.generateRequest();
    }

    manageAJAXResponse(e,request){
        if(this.transferButton){
            let data=request.response;
            let status=data.status;
            
            switch(status){
            case 200:
                let cuentas = [];
                let options = '';
                request.response.accounts.forEach(e => { cuentas.push(e.accountNumber) } );
                request.response.accounts.forEach(e => { 
                    //options.push('<option value="">' + e.accountNumber + '</option>' )
                    //options += '<option value="">' + e.accountNumber + '</option>';
                    options += '<option value="' + e.accountNumber + '"> ' + e.accountNumber + '</option>';
                    } );

                this.set('cuentas', cuentas);
                this.set('options', options);
                // let cuenta = request.response.accountNumber;
                break;
            }

            this.transferForm();

        } else {
            this.dataCards = request.response.accounts;
            //this.set('cuentas', cuentas);
        }
    }

    transferForm() {
        (async () => {
            const { value: formValues } = await Swal.fire({
                title: 'Transferencia',
                html:
                    '<label> Selecciona una de tus cuentas: </label>' + '<br>' + '<select class="form-control form-control-lg" name="cuentas" id="num-cuenta"> ' + this.options + ' </select>' + '<br><br>' +
                    '<div style="text-align:left;"><label> Ingresa el monto : </label><br>' + '<input type="number" id="monto" class="swal2-input" style="width:100%;">' + '<br><br>' +
                    '<label> Ingresa el número de cuenta que recibirá la transferencia : </label><br>' + '<input type="text" id="cuentaReceptora" class="swal2-input" style:"width:100%;">' + '<br><br>' +
                    '<label> Ingresa el password de tu cuenta : </label>' + '<input type="password" maxlength="16" id="password" class="swal2-input"></div>'
                ,
                focusConfirm: false,
                preConfirm: () => {
                    return [
                        document.getElementById('num-cuenta').value,
                        document.getElementById('monto').value,
                        document.getElementById('cuentaReceptora').value,
                        document.getElementById('password').value,
                    ]
                }
            })

            if (formValues) {
                this.generateTransfer(formValues);
            }
        })()
    }

    generateTransfer(formValues){
        if(!isEmpty(formValues[0])&&!isEmpty(formValues[1])&&!isEmpty(formValues[2])&&!isEmpty(formValues[3])) {
        let myemail = sessionStorage.getItem('Email');
        let mytoken = sessionStorage.getItem('Token');
        this.$.putTransfer.url = baseApi + 'transfers';
        this.$.putTransfer.headers = {
            "Access-Control-Allow-Origin":"*",
            "Authorization": 'Bearer ' + mytoken,
            "customer": myemail
        }

        let fecha = new Date();

        let transferData = {
            "amount" : parseFloat(formValues[1]) ,
            "date" : fecha, //"13/06/2020"
            "receiveAccount" : formValues [2], //3535635017667818, 
            "sendAccount" :   formValues[0], //5332278325201549,
            "password": formValues[3] // "password" 
        }
        
        this.$.putTransfer.body = JSON.stringify(transferData);
        this.$.putTransfer.generateRequest();
    }
    else{
        Swal.fire({
            icon: 'warning',
            title: 'Completa todos los campos!.',
        })
    }
    }

    manageAJAXTransferResponse(e,request){
        let data=request.response;
        let status=data.status;
        
        switch(status){
            case 200:
                Swal.fire({
                    icon: 'success',
                    title: 'Transferencia exitosa',
                })
            break;
        }
    }

    showError(e, error) {
        let errorResponse = e.detail.request.xhr.response;
        Toast.fire({icon: 'error',title: errorResponse});
    }

    showTransferError(e, error) {
        let errorResponse = e.detail.request.xhr.response.message;
        Swal.fire({
            icon: 'error',
            title: 'Error en la transferencia',
            text: errorResponse,
          })
    }

  
}

window.customElements.define('practitioner-profile', Profile);
