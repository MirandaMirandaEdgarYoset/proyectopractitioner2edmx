
/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

class Movements extends PolymerElement {
    static get properties() {
        return {
            inMovements:{type: Object},
            outMovements:{type: Object},

        };
    }

    static get template() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
       <div class="card">
            <h5 class="card-header">Tus Movimientos.</h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-12">
                        <table class="table table-hover table-sm table-responsive-sm">
                            <thead>
                                <tr class="bg-info">
                                    <th colspan="4">Movimientos entrantes </th>
                                </tr>
                                <tr>
                                    <th>Cuenta de envío</th>
                                    <th>Cuenta receptora</th>
                                    <th>Monto</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                <template is="dom-repeat" items="[[inMovements]]">
                                <tr>
                                    <td>{{format(item.sendAccount)}}</td> 
                                    <td>{{format(item.receiveAccount)}}</td>
                                    <td>$ {{item.amount.$numberDecimal}}</td>
                                    <td>{{item.date}}</td>
                                </tr>
                                </template>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-12">
                        <table class="table table-hover table-sm table-responsive-sm">
                            <thead>
                                <tr class="bg-info">
                                    <th colspan="4">Movimientos salientes </th>
                                </tr>
                                <tr>
                                    <th>Cuenta de envío</th>
                                    <th>Cuenta receptora</th>
                                    <th>Monto</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                <template is="dom-repeat" items="[[outMovements]]">
                                <tr>
                                    <td>{{format(item.sendAccount)}}</td> 
                                    <td>{{format(item.receiveAccount)}}</td>
                                    <td>$ {{item.amount.$numberDecimal}}</td>
                                    <td>{{item.date}}</td>
                                </tr>
                                </template>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <iron-ajax
            id=getMovements
            content-type="application/json"
            crossDomain: true
            method="GET"
            handle-as="json"
            headers="Access-Control-Allow-Headers: *"
            on-response="manageAJAXResponse"
            on-error="showError">
        </iron-ajax>
        `;
    }

    ready(){
        super.ready();
        let myemail = sessionStorage.getItem('Email');
        let mytoken = sessionStorage.getItem('Token');
        this.$.getMovements.url = baseApi+'movements';
        this.$.getMovements.headers = {
            "Access-Control-Allow-Origin":"*",
            "Authorization": 'Bearer ' + mytoken,
            "customer": myemail
        }
        this.$.getMovements.generateRequest();
    }

    manageAJAXResponse(e,request){
        let data=request.response;
        let status=data.status;

        //this.data = request.response.movements;
        //console.log("la variable data contiene: ", this.data);
        let movimientos = request.response.movements;

        if(movimientos){
            this.processMovementsData(movimientos);
        }

        switch(status){
        case 200:
            Toast.fire({icon: 'success',title: 'Movimientos obtenidos'});
            //this.dispatchEvent(new CustomEvent('logguedIn',{detail: {response:request.response}}));
            break;
        }
    }

    processMovementsData(movimientos){

        const mov = movimientos;
        // const singers = [
        //     { name: 'Steven Tyler', band: 'Aerosmith', born: 1948 },
        //     { name: 'Karen Carpenter', band: 'The Carpenters', born: 1950 },
        //     { name: 'Kurt Cobain', band: 'Nirvana', born: 1967 },
        //     { name: 'Stevie Nicks', band: 'Fleetwood Mac', born: 1948 },
        // ];

        function compareValues(key, order = 'asc') {
            return function innerSort(a, b) {
                if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
                    // property doesn't exist on either object
                    return 0;
                }

                const varA = (typeof a[key] === 'string')
                    ? a[key].toUpperCase() : a[key];
                const varB = (typeof b[key] === 'string')
                    ? b[key].toUpperCase() : b[key];

                let comparison = 0;
                if (varA > varB) {
                    comparison = 1;
                } else if (varA < varB) {
                    comparison = -1;
                }
                return (
                    (order === 'desc') ? (comparison * -1) : comparison
                );
            };
        }

        let customer = sessionStorage.getItem('customerId');
        let outMovements = mov.filter(function (el) {
            return el.customerId === customer;
          });
        this.outMovements = outMovements;


        let inMovements = mov.filter(function (el) {
            return el.customerId != customer;
          });
        console.log("el array filtrado por movimientos de entrada: ", inMovements);
        this.inMovements = inMovements;

        let arrayOrdenado = mov.sort(compareValues('customerId', 'desc'));
        this.data = mov;

    }



    showError(e,error){
        let errorResponse = e.detail.request.xhr.response;
        //Toast.fire({icon: 'error',title: errorResponse});
        //this.dispatchEvent(new CustomEvent('errorLogin',{detail: {request:error.request.status}}));
    }
    format(tarjeta){
        if(tarjeta==undefined||tarjeta==null)
          return "";
        let str=tarjeta.toString();
        let res = (str.match(/.{1,4}/g));
        return res.join('-');
      }

}

window.customElements.define('practitioner-movements', Movements);
