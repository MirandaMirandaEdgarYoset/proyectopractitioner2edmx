import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';


class CardMoney extends PolymerElement {
  static get template() {
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">    
    <div class="card text-white bg-info mb-3">
    <div class="card-header">Numero de tarjeta:</div>
    <div class="card-body">
        <p class="card-text">{{format(tarjeta)}} </p>
        <label>Saldo:</label>
        <p class="card-text">$ {{saldo}}</p>
     </div>
    </div>      

    `;
  }

  ready(){
    super.ready();
  }

  static get properties(){
    return{
      saldo:{
        type:String
      },
      tarjeta:{
        type:String
      },
      titulo:{
        type:String
      }
    }
  }

  format(tarjeta){
    if(tarjeta==undefined||tarjeta==null)
      return "";
    let str=tarjeta.toString();
    let res = (str.match(/.{1,4}/g));
    return res.join('-');
  }
  
}

window.customElements.define('card-money', CardMoney);
