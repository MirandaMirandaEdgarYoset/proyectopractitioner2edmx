/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../shared-styles.js';
import '../components/card-money.js'
import '../components/practitioner-login.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';

class AppLogin extends PolymerElement {
  static get template() {
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
    :host {
      --app-primary-color: #4285f4;
      --app-secondary-color: black;

      
    }

    app-drawer-layout:not([narrow]) [drawer-toggle] {
      display: none;
    }

    app-header {
      color: #fff;
      background-color: #061053;
    }

    app-header paper-icon-button {
      --paper-icon-button-ink-color: white;
    }

    .drawer-list {
      margin: 0 20px;
    }

    .drawer-list a {
      display: block;
      padding: 0 16px;
      text-decoration: none;
      color: var(--app-secondary-color);
      line-height: 40px;
    }
  </style>  
        <app-location route="{{route}}"></app-location>
        <app-route
            route="{{route}}"
            pattern="/:view"
            data="{{routeData}}"
            tail="{{subroute}}"></app-route>          
        <div id="principal" class="row">
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar>
              <div main-title="" style="text-align:center;">Mi Banca Online</div>
            </app-toolbar>
          </app-header>
        </app-header-layout>
      </app-drawer-layout>
      <div class="col-md-12">
          <practitioner-login id="loginComponent" titulo="Log in" subtitulo="Bienvenido" boton="Log in"></practitioner-login>
      </div>
    `;
  }
  ready(){
    super.ready();
    this.$.loginComponent.addEventListener('logguedIn',e=>{
      this.dispatchEvent(new CustomEvent('logguedInApp',{detail: {response:e.detail.response}}));
      this.set('route.path','dashboard');
    });
    this.$.loginComponent.addEventListener('errorLogin',e=>{
      this.dispatchEvent(new CustomEvent('errorLoginApp',{detail: {request:e.detail.request}}));
    });
    this.checkIsLoggueIn()
  }
  checkIsLoggueIn(){
    let customerId = sessionStorage.getItem('customerId');
    let token = sessionStorage.getItem('Token');
    let username = sessionStorage.getItem('UserName');
    let email = sessionStorage.getItem('Email');
    if(!isEmpty(customerId)&&!isEmpty(token)&&!isEmpty(email)&&!isEmpty(username)){
      this.set('route.path','dashboard')
    }
  }
}

window.customElements.define('app-login', AppLogin);
