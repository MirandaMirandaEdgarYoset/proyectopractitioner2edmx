/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icons/notification-icons.js';
import '@polymer/iron-icons/maps-icons';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class MyApp extends PolymerElement {
  static get template() {
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <style>
        :host {
          --app-primary-color: #4285f4;
          --app-secondary-color: black;

          
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }

        app-header {
          color: #fff;
          background-color: #061053;
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }

        .drawer-list {
          margin: 0 20px;
        }

        .drawer-list a {
          display: block;
          padding: 0 16px;
          text-decoration: none;
          color: var(--app-secondary-color);
          line-height: 40px;
        }

        .drawer-list a.iron-selected {
          color: black;
          font-weight: bold;
        }
        #menu{
          background-color: #061053;
          color:#fff;
        }
        #menu #selector{
         color:red;
        }
        .exit{
          margin-left:15px;
          width:150px;
        }
        .exit span{
          color:black;
        }
      </style>
      <app-location route="{{route}}"></app-location>
      <app-route
          route="{{route}}"
          pattern="/:view"
          data="{{routeData}}"
          tail="{{subroute}}"></app-route>
        <app-drawer-layout  class="drawer-all" fullbleed="" narrow="{{narrow}}">
          <!-- Drawer content -->
          <app-drawer id="drawer" slot="drawer" swipe-open="[[narrow]]">
            <app-toolbar id="menu">Menu</app-toolbar>
            <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
            <a name="profile" href="[[rootPath]]profile"><iron-icon icon="account-circle"></iron-icon> Mi Perfil</a>
            <a name="dashboard" href="[[rootPath]]dashboard"><iron-icon icon="home"></iron-icon> Home</a>
            <a name="movements" href="[[rootPath]]movements"><iron-icon icon="notification:sync"></iron-icon> Movimientos</a>
            <a name="atms" href="[[rootPath]]atms"><iron-icon icon="maps:place"></iron-icon> Sucursales y ATMS</a>
            <button type="button" class="exit btn btn-outline-warning btn-sm" on-click="logout"><span>Salir</span></button>
            </iron-selector>
            
          </app-drawer>
          <!-- Main content -->
          <app-header-layout has-scrolling-region="">
            <app-header  slot="header" id="header" condenses="" reveals="" effects="waterfall">
              <app-toolbar>
                <paper-icon-button icon="my-icons:menu" drawer-toggle=""></paper-icon-button>
                <div main-title="">Banca Online</div>
              </app-toolbar>
            </app-header>
             <slot></slot>
          </app-header-layout>
        </app-drawer-layout>
    `;
  }
  ready(){
    super.ready();
    this.checkIsLoggueIn();
  }
  logout(){
    sessionStorage.removeItem('customerId');
    sessionStorage.removeItem('Token');
    sessionStorage.removeItem('UserName');
    sessionStorage.removeItem('Email');
    location.replace(baseFront);
  }
  checkIsLoggueIn(){
    let customerId=sessionStorage.getItem('customerId');
    let token = sessionStorage.getItem('Token');
    let username = sessionStorage.getItem('UserName');
    let email = sessionStorage.getItem('Email');
    if(isEmpty(customerId)||isEmpty(token)||isEmpty(email)||isEmpty(username)){
      location.replace(baseFront);
    }
  }
}

window.customElements.define('my-app', MyApp);
