/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';
import {} from '@polymer/polymer/lib/elements/dom-if.js'
import '@webcomponents/webcomponentsjs/webcomponents-loader.js';
import '@johnriv/google-map/google-map.js'
import '@johnriv/google-map/google-map-marker.js'
import '@polymer/iron-ajax/iron-ajax.js';
import '../shared-styles.js';
import './my-app.js'

class AppAtms extends PolymerElement {
  static get properties() {
    return {
        api:{type:String,value:"AIzaSyAxNVayhHgcFDCnRBsv33nmh-qdja5iEIw"},
        token:{type:String,value:""},
        branches_atms:{type:Array},
        isAtms:{type:Boolean, value:false}
    };
  }
  static get template() {
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        .card{
          text-align:center;
          background-color: #B3E5FC;
        }
        google-map {
        position:relative !important;
        left:20%;
        height: 400px;
        width: 600px;
        border-radius: 10px;
        }    
      </style>
      <my-app>
      <div class="card">
      <h5 class="card-header">Mis Cajeros y Sucursales.</h5>
        <div class="card-body">
            <div class="row">
              <div class="col-md-12">
              <div class="btn-group btn-group-toggle" style="margin-bottom:10px;" data-toggle="buttons">
                  <label id="option1" class="btn btn-primary active">
                    <input type="radio" name="options" on-click="onclickOption1" autocomplete="off"><iron-icon icon="account-balance"></iron-icon>Sucursales
                  </label>
                  <label id="option2" class="btn btn-outline-primary">
                    <input type="radio" name="options" on-click="onclickOption2" autocomplete="off"><iron-icon icon="maps:local-atm"></iron-icon>Cajeros
                  </label>
                </div>
              </div>   
            </div>
            <div class="row">
              <div class="col col-xs-12 col-md-6 col-lg-12" style="text-align:center;">
              <google-map fit-to-markers latitude="19.432387" longitude="-99.132713" api-key="[[api]]" zoom="15" disable-default-ui>
                <template is=dom-repeat items="{{branches_atms}}">
                  <google-map-marker latitude="[[item.address.geolocation.latitude]]" longitude="[[item.address.geolocation.longitude]]" title="Go Giants!" draggable="false">
                  <div class="card" style="width: 12rem;">
                    <template is="dom-if" if="[[!isAtms]]" restamp>
                      <img class="card-img-top" src="../../images/branch.jpeg" width="100px" alt="Card image cap">
                    </template>
                    <template is="dom-if" if="[[isAtms]]" restamp>
                      <img class="card-img-top" src="../../images/atm.jpg" width="100px" alt="Card image cap">
                    </template>
                    <div class="card-body">
                      <label>{{item.address.country.name}} {{item.address.state.name}}</label>
                       <p>{{item.description}} {{item.address.city}}</p>
                       <p>{{item.address.addressName}}</p>
                    </div>
                  </div>  
                  </google-map-marker>
                </template>
              </google-map>
              </div>   
            </div>
        </div>
    <iron-ajax
          id="getAtmsOrBranches"
          content-type="application/json"
          crossDomain: true
          method="GET",
          handle-as="json"
          on-response="manageAJAXResponse"
          on-error="showError"></iron-ajax>
      </div>
      </my-app>
    `;
  }
  ready(){
    super.ready();
    console.log('page atms');
    this.checkToken();
    this.initMap();
  }
  checkToken(){
    let mytoken = sessionStorage.getItem('Token');
    if(!isEmpty(mytoken)){
      this.token='Bearer '+mytoken;
    }
  }
  initMap(){
    let myemail=sessionStorage.getItem('Email');
    if(this.isAtms){
      this.$.getAtmsOrBranches.url=baseApi+'atms'
    }
    else{
      this.$.getAtmsOrBranches.url=baseApi+'branches'
    }
    this.$.getAtmsOrBranches.headers={"Access-Control-Allow-Origin":"*","Authorization": this.token,"customer": myemail}
    this.$.getAtmsOrBranches.generateRequest();
  }

  onclickOption1(){
    this.isAtms=false;
    this.$.option1.className="btn btn-primary active";
    this.$.option2.className="btn btn-outline-primary";
    this.initMap();
  }

  onclickOption2(){
    this.isAtms=true;
    this.$.option2.className="btn btn-primary active";
    this.$.option1.className="btn btn-outline-primary";
    this.initMap();
  }

  manageAJAXResponse(e,request){
    this.branches_atms=request.response.data;
  }

  showError(e,error){
    Toast.fire({icon: 'error',title: 'Error al consultar los Atms y sucursales.'});
  }
  
}

window.customElements.define('app-atms', AppAtms);
