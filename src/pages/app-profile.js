/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../shared-styles.js';
import '../components/card-money.js'
import '../components/practitioner-profile.js';
import './my-app.js'

class AppProfile extends PolymerElement {
  static get template() {
    return html`

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style include="shared-styles">
      :host {
        display: block;

        padding: 10px;
      }
      .card{
        background-color: #B3E5FC;
        color: #000000;
      }
    </style>
    <my-app>
    <div class="card" class="col-md-12">
      <practitioner-profile></practitioner-profile>
    </div>
    </my-app>
    `;
  }
}

window.customElements.define('app-profile', AppProfile);
