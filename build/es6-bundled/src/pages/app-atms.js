define(["exports","../my-appnav.js","./my-app.js"],function(_exports,_myAppnav,_myApp){"use strict";Object.defineProperty(_exports,"__esModule",{value:!0});_exports.IronJsonpLibraryBehavior=_exports.$ironJsonpLibrary=void 0;const IronJsonpLibraryBehavior={properties:{/**
     * True if library has been successfully loaded
     */libraryLoaded:{type:Boolean,value:!1,notify:!0,readOnly:!0},/**
     * Not null if library has failed to load
     */libraryErrorMessage:{type:String,value:null,notify:!0,readOnly:!0// Following properties are to be set by behavior users
/**
       * Library url. Must contain string `%%callback%%`.
       *
       * `%%callback%%` is a placeholder for jsonp wrapper function name
       *
       * Ex: https://maps.googleapis.com/maps/api/js?callback=%%callback%%
       * @property libraryUrl
       */ /**
           * Set if library requires specific callback name.
           * Name will be automatically generated if not set.
           * @property callbackName
           */ /**
               * name of event to be emitted when library loads. Standard is `api-load`
               * @property notifyEvent
               */ /**
                   * event with name specified in `notifyEvent` attribute
                   * will fire upon successful load2
                   * @event `notifyEvent`
                   */}},observers:["_libraryUrlChanged(libraryUrl)"],_libraryUrlChanged:function(libraryUrl){// can't load before ready because notifyEvent might not be set
if(this._isReady&&this.libraryUrl)this._loadLibrary()},_libraryLoadCallback:function(err,result){if(err){_myAppnav.Base._warn("Library load failed:",err.message);this._setLibraryErrorMessage(err.message)}else{this._setLibraryErrorMessage(null);this._setLibraryLoaded(!0);if(this.notifyEvent)this.fire(this.notifyEvent,result,{composed:!0})}},/** loads the library, and fires this.notifyEvent upon completion */_loadLibrary:function(){LoaderMap.require(this.libraryUrl,this._libraryLoadCallback.bind(this),this.callbackName)},ready:function(){this._isReady=!0;if(this.libraryUrl)this._loadLibrary()}};/**
    * LoaderMap keeps track of all Loaders
    */_exports.IronJsonpLibraryBehavior=IronJsonpLibraryBehavior;var LoaderMap={apiMap:{},// { hash -> Loader }
/**
   * @param {Function} notifyCallback loaded callback fn(result)
   * @param {string} jsonpCallbackName name of jsonpcallback. If API does not provide it, leave empty. Optional.
   */require:function(url,notifyCallback,jsonpCallbackName){// make hashable string form url
var name=this.nameFromUrl(url);// create a loader as needed
if(!this.apiMap[name])this.apiMap[name]=new Loader(name,url,jsonpCallbackName);// ask for notification
this.apiMap[name].requestNotify(notifyCallback)},nameFromUrl:function(url){return url.replace(/[\:\/\%\?\&\.\=\-\,]/g,"_")+"_api"}},Loader=function(name,url,callbackName){this.notifiers=[];// array of notifyFn [ notifyFn* ]
// callback is specified either as callback name
// or computed dynamically if url has callbackMacro in it
if(!callbackName){if(0<=url.indexOf(this.callbackMacro)){callbackName=name+"_loaded";url=url.replace(this.callbackMacro,callbackName)}else{this.error=new Error("IronJsonpLibraryBehavior a %%callback%% parameter is required in libraryUrl");// TODO(sjmiles): we should probably fallback to listening to script.load
return}}this.callbackName=callbackName;window[this.callbackName]=this.success.bind(this);this.addScript(url)};/** @constructor */Loader.prototype={callbackMacro:"%%callback%%",loaded:!1,addScript:function(src){var script=document.createElement("script");script.src=src;script.onerror=this.handleError.bind(this);var s=document.querySelector("script")||document.body;s.parentNode.insertBefore(script,s);this.script=script},removeScript:function(){if(this.script.parentNode){this.script.parentNode.removeChild(this.script)}this.script=null},handleError:function(ev){this.error=new Error("Library failed to load");this.notifyAll();this.cleanup()},success:function(){this.loaded=!0;this.result=Array.prototype.slice.call(arguments);this.notifyAll();this.cleanup()},cleanup:function(){delete window[this.callbackName]},notifyAll:function(){this.notifiers.forEach(function(notifyCallback){notifyCallback(this.error,this.result)}.bind(this));this.notifiers=[]},requestNotify:function(notifyCallback){if(this.loaded||this.error){notifyCallback(this.error,this.result)}else{this.notifiers.push(notifyCallback)}}};/**
     Loads specified jsonp library.
   
     Example:
   
         <iron-jsonp-library
           library-url="https://apis.google.com/js/plusone.js?onload=%%callback%%"
           notify-event="api-load"
           library-loaded="{{loaded}}"></iron-jsonp-library>
   
     Will emit 'api-load' event when loaded, and set 'loaded' to true
   
     Implemented by  Polymer.IronJsonpLibraryBehavior. Use it
     to create specific library loader elements.
   
     @demo demo/index.html
   */(0,_myAppnav.Polymer)({is:"iron-jsonp-library",behaviors:[IronJsonpLibraryBehavior],properties:{/**
     * Library url. Must contain string `%%callback%%`.
     *
     * `%%callback%%` is a placeholder for jsonp wrapper function name
     *
     * Ex: https://maps.googleapis.com/maps/api/js?callback=%%callback%%
     */libraryUrl:String,/**
     * Set if library requires specific callback name.
     * Name will be automatically generated if not set.
     */callbackName:String,/**
     * event with name specified in 'notifyEvent' attribute
     * will fire upon successful load
     */notifyEvent:String/**
                            * event with name specified in 'notifyEvent' attribute
                            * will fire upon successful load
                            * @event `notifyEvent`
                            */}});var ironJsonpLibrary={IronJsonpLibraryBehavior:IronJsonpLibraryBehavior};_exports.$ironJsonpLibrary=ironJsonpLibrary;(0,_myAppnav.Polymer)({is:"google-maps-api",behaviors:[IronJsonpLibraryBehavior],properties:{/** @private */mapsUrl:{type:String,value:"https://maps.googleapis.com/maps/api/js?callback=%%callback%%"},/**
     * A Maps API key. To obtain an API key, see developers.google.com/maps/documentation/javascript/tutorial#api_key.
     */apiKey:{type:String,value:""},/**
     * A Maps API for Business Client ID. To obtain a Maps API for Business Client ID, see developers.google.com/maps/documentation/business/.
     * If set, a Client ID will take precedence over an API Key.
     */clientId:{type:String,value:""},/**
     * Version of the Maps API to use.
     */version:{type:String,value:"3.exp"},/**
     * The localized language to load the Maps API with. For more information
     * see https://developers.google.com/maps/documentation/javascript/basics#Language
     *
     * Note: the Maps API defaults to the preffered language setting of the browser.
     * Use this parameter to override that behavior.
     */language:{type:String,value:""},/**
     * If true, sign-in is enabled.
     * See https://developers.google.com/maps/documentation/javascript/signedin#enable_sign_in
     */signedIn:{type:Boolean,value:!1},/**
     * Fired when the Maps API library is loaded and ready.
     * @event api-load
     */ /**
         * Name of event fired when library is loaded and available.
         */notifyEvent:{type:String,value:"api-load"},/** @private */libraryUrl:{type:String,computed:"_computeUrl(mapsUrl, version, apiKey, clientId, language, signedIn)"}},_computeUrl(mapsUrl,version,apiKey,clientId,language,signedIn){let url=`${mapsUrl}&v=${version}`;// Always load all Maps API libraries.
url+="&libraries=drawing,geometry,places,visualization";if(apiKey&&!clientId){url+=`&key=${apiKey}`}if(clientId){url+=`&client=${clientId}`}// Log a warning if the user is not using an API Key or Client ID.
if(!apiKey&&!clientId){const warning="No Google Maps API Key or Client ID specified. "+"See https://developers.google.com/maps/documentation/javascript/get-api-key "+"for instructions to get started with a key or client id.";console.warn(warning)}if(language){url+=`&language=${language}`}if(signedIn){url+=`&signed_in=${signedIn}`}return url},/**
   * Provides the google.maps JS API namespace.
   */get api(){return google.maps}});function setupDragHandler_(){if(this.draggable){this.dragHandler_=google.maps.event.addListener(this.marker,"dragend",onDragEnd_.bind(this))}else{google.maps.event.removeListener(this.dragHandler_);this.dragHandler_=null}}function onDragEnd_(e,details,sender){this.latitude=e.latLng.lat();this.longitude=e.latLng.lng()}(0,_myAppnav.Polymer)({_template:_myAppnav.html$1`
    <style>
      :host {
        display: none;
      }
    </style>

    <slot></slot>
`,is:"google-map-marker",/**
   * Fired when the marker icon was clicked. Requires the clickEvents attribute to be true.
   *
   * @param {google.maps.MouseEvent} event The mouse event.
   * @event google-map-marker-click
   */ /**
       * Fired when the marker icon was double clicked. Requires the clickEvents attribute to be true.
       *
       * @param {google.maps.MouseEvent} event The mouse event.
       * @event google-map-marker-dblclick
       */ /**
           * Fired repeatedly while the user drags the marker. Requires the dragEvents attribute to be true.
           *
           * @event google-map-marker-drag
           */ /**
               * Fired when the user stops dragging the marker. Requires the dragEvents attribute to be true.
               *
               * @event google-map-marker-dragend
               */ /**
                   * Fired when the user starts dragging the marker. Requires the dragEvents attribute to be true.
                   *
                   * @event google-map-marker-dragstart
                   */ /**
                       * Fired for a mousedown on the marker. Requires the mouseEvents attribute to be true.
                       *
                       * @event google-map-marker-mousedown
                       * @param {google.maps.MouseEvent} event The mouse event.
                       */ /**
                           * Fired when the DOM `mousemove` event is fired on the marker. Requires the mouseEvents
                           * attribute to be true.
                           *
                           * @event google-map-marker-mousemove
                           * @param {google.maps.MouseEvent} event The mouse event.
                           */ /**
                               * Fired when the mouse leaves the area of the marker icon. Requires the mouseEvents attribute to be
                               * true.
                               *
                               * @event google-map-marker-mouseout
                               * @param {google.maps.MouseEvent} event The mouse event.
                               */ /**
                                   * Fired when the mouse enters the area of the marker icon. Requires the mouseEvents attribute to be
                                   * true.
                                   *
                                   * @event google-map-marker-mouseover
                                   * @param {google.maps.MouseEvent} event The mouse event.
                                   */ /**
                                       * Fired for a mouseup on the marker. Requires the mouseEvents attribute to be true.
                                       *
                                       * @event google-map-marker-mouseup
                                       * @param {google.maps.MouseEvent} event The mouse event.
                                       */ /**
                                           * Fired for a rightclick on the marker. Requires the clickEvents attribute to be true.
                                           *
                                           * @event google-map-marker-rightclick
                                           * @param {google.maps.MouseEvent} event The mouse event.
                                           */ /**
                                               * Fired when an infowindow is opened.
                                               *
                                               * @event google-map-marker-open
                                               */ /**
                                                   * Fired when the close button of the infowindow is pressed.
                                                   *
                                                   * @event google-map-marker-close
                                                   */properties:{/**
     * A Google Maps marker object.
     *
     * @type google.maps.Marker
     */marker:{type:Object,notify:!0},/**
     * The Google map object.
     *
     * @type google.maps.Map
     */map:{type:Object,observer:"_mapChanged"},/**
     * A Google Map Infowindow object.
     *
     * @type {?Object}
     */info:{type:Object,value:null},/**
     * When true, marker *click events are automatically registered.
     */clickEvents:{type:Boolean,value:!1,observer:"_clickEventsChanged"},/**
     * When true, marker drag* events are automatically registered.
     */dragEvents:{type:Boolean,value:!1,observer:"_dragEventsChanged"},/**
     * Image URL for the marker icon.
     *
     * @type string|google.maps.Icon|google.maps.Symbol
     */icon:{type:Object,value:null,observer:"_iconChanged"},/**
     * When true, marker mouse* events are automatically registered.
     */mouseEvents:{type:Boolean,value:!1,observer:"_mouseEventsChanged"},/**
     * Z-index for the marker icon.
     */zIndex:{type:Number,value:0,observer:"_zIndexChanged"},/**
     * The marker's longitude coordinate.
     */longitude:{type:Number,value:null,notify:!0},/**
     * The marker's latitude coordinate.
     */latitude:{type:Number,value:null,notify:!0},/**
     * The marker's label.
     */label:{type:String,value:null,observer:"_labelChanged"},/**
     * A animation for the marker. "DROP" or "BOUNCE". See
     * https://developers.google.com/maps/documentation/javascript/examples/marker-animations.
     */animation:{type:String,value:null,observer:"_animationChanged"},/**
     * Specifies whether the InfoWindow is open or not
     */open:{type:Boolean,value:!1,observer:"_openChanged"}},observers:["_updatePosition(latitude, longitude)"],detached(){if(this.marker){google.maps.event.clearInstanceListeners(this.marker);this._listeners={};this.marker.setMap(null)}if(this._contentObserver){this._contentObserver.disconnect()}},attached(){// If element is added back to DOM, put it back on the map.
if(this.marker){this.marker.setMap(this.map)}},_updatePosition(){if(this.marker&&null!=this.latitude&&null!=this.longitude){this.marker.setPosition(new google.maps.LatLng(parseFloat(this.latitude),parseFloat(this.longitude)))}},_clickEventsChanged(){if(this.map){if(this.clickEvents){this._forwardEvent("click");this._forwardEvent("dblclick");this._forwardEvent("rightclick")}else{this._clearListener("click");this._clearListener("dblclick");this._clearListener("rightclick")}}},_dragEventsChanged(){if(this.map){if(this.dragEvents){this._forwardEvent("drag");this._forwardEvent("dragend");this._forwardEvent("dragstart")}else{this._clearListener("drag");this._clearListener("dragend");this._clearListener("dragstart")}}},_mouseEventsChanged(){if(this.map){if(this.mouseEvents){this._forwardEvent("mousedown");this._forwardEvent("mousemove");this._forwardEvent("mouseout");this._forwardEvent("mouseover");this._forwardEvent("mouseup")}else{this._clearListener("mousedown");this._clearListener("mousemove");this._clearListener("mouseout");this._clearListener("mouseover");this._clearListener("mouseup")}}},_animationChanged(){if(this.marker){this.marker.setAnimation(google.maps.Animation[this.animation])}},_labelChanged(){if(this.marker){this.marker.setLabel(this.label)}},_iconChanged(){if(this.marker){this.marker.setIcon(this.icon)}},_zIndexChanged(){if(this.marker){this.marker.setZIndex(this.zIndex)}},_mapChanged(){// Marker will be rebuilt, so disconnect existing one from old map and listeners.
if(this.marker){this.marker.setMap(null);google.maps.event.clearInstanceListeners(this.marker)}if(this.map&&this.map instanceof google.maps.Map){this._mapReady()}},_contentChanged(){if(this._contentObserver){this._contentObserver.disconnect()}// Watch for future updates.
this._contentObserver=new MutationObserver(this._contentChanged.bind(this));this._contentObserver.observe(this,{childList:!0,subtree:!0});const content=this.innerHTML.trim();if(content){if(!this.info){// Create a new infowindow
this.info=new google.maps.InfoWindow;this.openInfoHandler_=google.maps.event.addListener(this.marker,"click",()=>{this.open=!0});this.closeInfoHandler_=google.maps.event.addListener(this.info,"closeclick",()=>{this.open=!1})}this.info.setContent(content)}else if(this.info){// Destroy the existing infowindow.  It doesn't make sense to have an empty one.
google.maps.event.removeListener(this.openInfoHandler_);google.maps.event.removeListener(this.closeInfoHandler_);this.info=null}},_openChanged(){if(this.info){if(this.open){this.info.open(this.map,this.marker);this.fire("google-map-marker-open")}else{this.info.close();this.fire("google-map-marker-close")}}},_mapReady(){this._listeners={};this.marker=new google.maps.Marker({map:this.map,position:{lat:parseFloat(this.latitude),lng:parseFloat(this.longitude)},title:this.title,animation:google.maps.Animation[this.animation],draggable:this.draggable,visible:!this.hidden,icon:this.icon,label:this.label,zIndex:this.zIndex});this._contentChanged();this._clickEventsChanged();this._dragEventsChanged();this._mouseEventsChanged();this._openChanged();setupDragHandler_.bind(this)()},_clearListener(name){if(this._listeners[name]){google.maps.event.removeListener(this._listeners[name]);this._listeners[name]=null}},_forwardEvent(name){this._listeners[name]=google.maps.event.addListener(this.marker,name,event=>{this.fire(`google-map-marker-${name}`,event)})},attributeChanged(attrName){if(!this.marker){return}// Cannot use *Changed watchers for native properties.
switch(attrName){case"hidden":this.marker.setVisible(!this.hidden);break;case"draggable":this.marker.setDraggable(this.draggable);setupDragHandler_.bind(this)();break;case"title":this.marker.setTitle(this.title);break;}}});(0,_myAppnav.Polymer)({_template:_myAppnav.html$1`
    <style>
      :host {
        position: relative;
        display: block;
        height: 100%;
      }

      #map {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
      }
    </style>

    <google-maps-api id="api" api-key="[[apiKey]]" client-id="[[clientId]]" version="[[version]]" signed-in="[[signedIn]]" language="[[language]]" on-api-load="_mapApiLoaded" maps-url="[[mapsUrl]]">
    </google-maps-api>

    <div id="map"></div>

    <iron-selector id="selector" multi="[[!singleInfoWindow]]" selected-attribute="open" activate-event="google-map-marker-open" on-google-map-marker-close="_deselectMarker">
      <slot id="markers" name="markers"></slot>
    </iron-selector>

    <slot id="objects"></slot>
`,is:"google-map",/**
   * Fired when the Maps API has fully loaded.
   *
   * @event google-map-ready
   */ /**
       * Fired when the user clicks on the map (but not when they click on a marker, infowindow, or
       * other object). Requires the clickEvents attribute to be true.
       *
       * @event google-map-click
       * @param {google.maps.MouseEvent} event The mouse event.
       */ /**
           * Fired when the user double-clicks on the map. Note that the google-map-click event will also fire,
           * right before this one. Requires the clickEvents attribute to be true.
           *
           * @event google-map-dblclick
           * @param {google.maps.MouseEvent} event The mouse event.
           */ /**
               * Fired repeatedly while the user drags the map. Requires the dragEvents attribute to be true.
               *
               * @event google-map-drag
               */ /**
                   * Fired when the user stops dragging the map. Requires the dragEvents attribute to be true.
                   *
                   * @event google-map-dragend
                   */ /**
                       * Fired when the user starts dragging the map. Requires the dragEvents attribute to be true.
                       *
                       * @event google-map-dragstart
                       */ /**
                           * Fired whenever the user's mouse moves over the map container. Requires the mouseEvents attribute to
                           * be true.
                           *
                           * @event google-map-mousemove
                           * @param {google.maps.MouseEvent} event The mouse event.
                           */ /**
                               * Fired when the user's mouse exits the map container. Requires the mouseEvents attribute to be true.
                               *
                               * @event google-map-mouseout
                               * @param {google.maps.MouseEvent} event The mouse event.
                               */ /**
                                   * Fired when the user's mouse enters the map container. Requires the mouseEvents attribute to be true.
                                   *
                                   * @event google-map-mouseover
                                   * @param {google.maps.MouseEvent} event The mouse event.
                                   */ /**
                                       * Fired when the DOM `contextmenu` event is fired on the map container. Requires the clickEvents
                                       * attribute to be true.
                                       *
                                       * @event google-map-rightclick
                                       * @param {google.maps.MouseEvent} event The mouse event.
                                       */ /**
                                           * Fired when the map becomes idle after panning or zooming.
                                           *
                                           * @event google-map-idle
                                           */ /**
                                               * Polymer properties for the google-map custom element.
                                               */properties:{/**
     * A Maps API key. To obtain an API key, see https://developers.google.com/maps/documentation/javascript/tutorial#api_key.
     */apiKey:String,/**
     * Overrides the origin the Maps API is loaded from. Defaults to `https://maps.googleapis.com`.
     */mapsUrl:{type:String// Initial value set in google-maps-api.
},/**
     * A Maps API for Business Client ID. To obtain a Maps API for Business Client ID, see https://developers.google.com/maps/documentation/business/.
     * If set, a Client ID will take precedence over an API Key.
     */clientId:String,/**
     * A latitude to center the map on.
     */latitude:{type:Number,value:37.77493,notify:!0,reflectToAttribute:!0},/**
     * A Maps API object.
     */map:{type:Object,notify:!0,value:null},/**
     * A longitude to center the map on.
     */longitude:{type:Number,value:-122.41942,notify:!0,reflectToAttribute:!0},/**
     * A kml file to load.
     */kml:{type:String,value:null,observer:"_loadKml"},/**
     * A zoom level to set the map to.
     */zoom:{type:Number,value:10,observer:"_zoomChanged",notify:!0},/**
     * When set, prevents the map from tilting (when the zoom level and viewport supports it).
     */noAutoTilt:{type:Boolean,value:!1},/**
     * Map type to display. One of 'roadmap', 'satellite', 'hybrid', 'terrain'.
     */mapType:{type:String,value:"roadmap",// roadmap, satellite, hybrid, terrain,
observer:"_mapTypeChanged",notify:!0},/**
     * Version of the Google Maps API to use.
     */version:{type:String,value:"3.exp"},/**
     * If set, removes the map's default UI controls.
     */disableDefaultUi:{type:Boolean,value:!1,observer:"_disableDefaultUiChanged"},/**
     * If set, removes the map's 'map type' UI controls.
     */disableMapTypeControl:{type:Boolean,value:!1,observer:"_disableMapTypeControlChanged"},/**
     * If set, removes the map's 'street view' UI controls.
     */disableStreetViewControl:{type:Boolean,value:!1,observer:"_disableStreetViewControlChanged"},/**
     * If set, the zoom level is set such that all markers (google-map-marker children) are brought into view.
     */fitToMarkers:{type:Boolean,value:!1,observer:"_fitToMarkersChanged"},/**
     * If true, prevent the user from zooming the map interactively.
     */disableZoom:{type:Boolean,value:!1,observer:"_disableZoomChanged"},/**
     * If set, custom styles can be applied to the map.
     * For style documentation see https://developers.google.com/maps/documentation/javascript/reference#MapTypeStyle
     */styles:{type:Object,value(){return{}}},/**
     * A maximum zoom level which will be displayed on the map.
     */maxZoom:{type:Number,observer:"_maxZoomChanged"},/**
     * A minimum zoom level which will be displayed on the map.
     */minZoom:{type:Number,observer:"_minZoomChanged"},/**
     * If true, sign-in is enabled.
     * See https://developers.google.com/maps/documentation/javascript/signedin#enable_sign_in
     */signedIn:{type:Boolean,value:!1},/**
     * The localized language to load the Maps API with. For more information
     * see https://developers.google.com/maps/documentation/javascript/basics#Language
     *
     * Note: the Maps API defaults to the preffered language setting of the browser.
     * Use this parameter to override that behavior.
     */language:{type:String},/**
     * When true, map *click events are automatically registered.
     */clickEvents:{type:Boolean,value:!1,observer:"_clickEventsChanged"},/**
     * When true, map drag* events are automatically registered.
     */dragEvents:{type:Boolean,value:!1,observer:"_dragEventsChanged"},/**
     * When true, map mouse* events are automatically registered.
     */mouseEvents:{type:Boolean,value:!1,observer:"_mouseEventsChanged"},/**
     * Additional map options for google.maps.Map constructor.
     * Use to specify additional options we do not expose as
     * properties.
     * Ex: `<google-map additional-map-options='{"mapTypeId":"satellite"}'>`
     *
     * Note, you can't use API enums like `google.maps.ControlPosition.TOP_RIGHT`
     * when using this property as an HTML attribute. Instead, use the actual
     * value (e.g. `3`) or set `.additionalMapOptions` in JS rather than using
     * the attribute.
     */additionalMapOptions:{type:Object,value(){return{}}},/**
     * The markers on the map.
     */markers:{type:Array,value(){return[]},readOnly:!0},/**
     * The non-marker objects on the map.
     */objects:{type:Array,value(){return[]},readOnly:!0},/**
     * If set, all other info windows on markers are closed when opening a new one.
     */singleInfoWindow:{type:Boolean,value:!1}},listeners:{"iron-resize":"resize"},observers:["_debounceUpdateCenter(latitude, longitude)"],attached(){this._initGMap()},detached(){if(this._markersChildrenListener){this.unlisten(this.$.selector,"items-changed","_updateMarkers");this._markersChildrenListener=null}if(this._objectsMutationObserver){this._objectsMutationObserver.disconnect();this._objectsMutationObserver=null}},behaviors:[_myAppnav.IronResizableBehavior],_initGMap(){if(this.map){return;// already initialized
}if(!0!==this.$.api.libraryLoaded){return;// api not loaded
}if(!this.isAttached){return;// not attached
}this.map=new google.maps.Map(this.$.map,this._getMapOptions());this._listeners={};this._updateCenter();this._loadKml();this._updateMarkers();this._updateObjects();this._addMapListeners();this.fire("google-map-ready")},_mapApiLoaded(){this._initGMap()},_getMapOptions(){const mapOptions={zoom:this.zoom,tilt:this.noAutoTilt?0:45,mapTypeId:this.mapType,disableDefaultUI:this.disableDefaultUi,mapTypeControl:!this.disableDefaultUi&&!this.disableMapTypeControl,streetViewControl:!this.disableDefaultUi&&!this.disableStreetViewControl,disableDoubleClickZoom:this.disableZoom,scrollwheel:!this.disableZoom,styles:this.styles,maxZoom:+this.maxZoom,minZoom:+this.minZoom};// Only override the default if set.
// We use getAttribute here because the default value of this.draggable = false even when not set.
if(null!=this.getAttribute("draggable")){mapOptions.draggable=this.draggable}for(const p in this.additionalMapOptions){mapOptions[p]=this.additionalMapOptions[p]}return mapOptions},_attachChildrenToMap(children){if(this.map){for(var i=0,child;child=children[i];++i){child.map=this.map}}},// watch for future updates to marker objects
_observeMarkers(){// Watch for future updates.
if(this._markersChildrenListener){return}this._markersChildrenListener=this.listen(this.$.selector,"items-changed","_updateMarkers")},_updateMarkers(){const newMarkers=Array.prototype.slice.call(this.$.markers.assignedNodes({flatten:!0}));// do not recompute if markers have not been added or removed
if(newMarkers.length===this.markers.length){const added=newMarkers.filter(m=>this.markers&&-1===this.markers.indexOf(m));if(0===added.length){// set up observer first time around
if(!this._markersChildrenListener){this._observeMarkers()}return}}this._observeMarkers();this.markers=this._setMarkers(newMarkers);// Set the map on each marker and zoom viewport to ensure they're in view.
this._attachChildrenToMap(this.markers);if(this.fitToMarkers){this._fitToMarkersChanged()}},// watch for future updates to non-marker objects
_observeObjects(){if(this._objectsMutationObserver){return}this._objectsMutationObserver=new MutationObserver(this._updateObjects.bind(this));this._objectsMutationObserver.observe(this,{childList:!0})},_updateObjects(){const newObjects=Array.prototype.slice.call(this.$.objects.assignedNodes({flatten:!0}));// Do not recompute if objects have not been added or removed.
if(newObjects.length===this.objects.length){const added=newObjects.filter(o=>-1===this.objects.indexOf(o));if(0===added.length){// Set up observer first time around.
this._observeObjects();return}}this._observeObjects();this._setObjects(newObjects);this._attachChildrenToMap(this.objects)},/**
   * Clears all markers from the map.
   *
   * @method clear
   */clear(){for(var i=0,m;m=this.markers[i];++i){m.marker.setMap(null)}},/**
   * Explicitly resizes the map, updating its center. This is useful if the
   * map does not show after you have unhidden it.
   *
   * @method resize
   */resize(){if(this.map){// saves and restores latitude/longitude because resize can move the center
const oldLatitude=this.latitude,oldLongitude=this.longitude;google.maps.event.trigger(this.map,"resize");this.latitude=oldLatitude;// restore because resize can move our center
this.longitude=oldLongitude;if(this.fitToMarkers){// we might not have a center if we are doing fit-to-markers
this._fitToMarkersChanged()}}},_loadKml(){if(this.map&&this.kml){const kmlfile=new google.maps.KmlLayer({url:this.kml,map:this.map})}},_debounceUpdateCenter(){this.debounce("updateCenter",this._updateCenter)},_updateCenter(){this.cancelDebouncer("updateCenter");if(this.map&&this.latitude!==void 0&&this.longitude!==void 0){// allow for latitude and longitude to be String-typed, but still Number valued
const lati=+this.latitude;if(isNaN(lati)){throw new TypeError("latitude must be a number")}const longi=+this.longitude;if(isNaN(longi)){throw new TypeError("longitude must be a number")}const newCenter=new google.maps.LatLng(lati,longi);let oldCenter=this.map.getCenter();if(!oldCenter){// If the map does not have a center, set it right away.
this.map.setCenter(newCenter)}else{// Using google.maps.LatLng returns corrected lat/lngs.
oldCenter=new google.maps.LatLng(oldCenter.lat(),oldCenter.lng());// If the map currently has a center, slowly pan to the new one.
if(!oldCenter.equals(newCenter)){this.map.panTo(newCenter)}}}},_zoomChanged(){if(this.map){this.map.setZoom(+this.zoom)}},_idleEvent(){if(this.map){this._forwardEvent("idle")}else{this._clearListener("idle")}},_clickEventsChanged(){if(this.map){if(this.clickEvents){this._forwardEvent("click");this._forwardEvent("dblclick");this._forwardEvent("rightclick")}else{this._clearListener("click");this._clearListener("dblclick");this._clearListener("rightclick")}}},_dragEventsChanged(){if(this.map){if(this.dragEvents){this._forwardEvent("drag");this._forwardEvent("dragend");this._forwardEvent("dragstart")}else{this._clearListener("drag");this._clearListener("dragend");this._clearListener("dragstart")}}},_mouseEventsChanged(){if(this.map){if(this.mouseEvents){this._forwardEvent("mousemove");this._forwardEvent("mouseout");this._forwardEvent("mouseover")}else{this._clearListener("mousemove");this._clearListener("mouseout");this._clearListener("mouseover")}}},_maxZoomChanged(){if(this.map){this.map.setOptions({maxZoom:+this.maxZoom})}},_minZoomChanged(){if(this.map){this.map.setOptions({minZoom:+this.minZoom})}},_mapTypeChanged(){if(this.map){this.map.setMapTypeId(this.mapType)}},_disableDefaultUiChanged(){if(!this.map){return}this.map.setOptions({disableDefaultUI:this.disableDefaultUi})},_disableMapTypeControlChanged(){if(!this.map){return}this.map.setOptions({mapTypeControl:!this.disableMapTypeControl})},_disableStreetViewControlChanged(){if(!this.map){return}this.map.setOptions({streetViewControl:!this.disableStreetViewControl})},_disableZoomChanged(){if(!this.map){return}this.map.setOptions({disableDoubleClickZoom:this.disableZoom,scrollwheel:!this.disableZoom})},attributeChanged(attrName){if(!this.map){return}// Cannot use *Changed watchers for native properties.
switch(attrName){case"draggable":this.map.setOptions({draggable:this.draggable});break;}},_fitToMarkersChanged(){// TODO(ericbidelman): respect user's zoom level.
if(this.map&&this.fitToMarkers&&0<this.markers.length){const latLngBounds=new google.maps.LatLngBounds;for(var i=0,m;m=this.markers[i];++i){latLngBounds.extend(new google.maps.LatLng(m.latitude,m.longitude))}// For one marker, don't alter zoom, just center it.
if(1<this.markers.length){this.map.fitBounds(latLngBounds)}this.map.setCenter(latLngBounds.getCenter())}},_addMapListeners(){google.maps.event.addListener(this.map,"center_changed",()=>{const center=this.map.getCenter();this.latitude=center.lat();this.longitude=center.lng()});google.maps.event.addListener(this.map,"zoom_changed",()=>{this.zoom=this.map.getZoom()});google.maps.event.addListener(this.map,"maptypeid_changed",()=>{this.mapType=this.map.getMapTypeId()});this._clickEventsChanged();this._dragEventsChanged();this._mouseEventsChanged();this._idleEvent()},_clearListener(name){if(this._listeners[name]){google.maps.event.removeListener(this._listeners[name]);this._listeners[name]=null}},_forwardEvent(name){this._listeners[name]=google.maps.event.addListener(this.map,name,event=>{this.fire(`google-map-${name}`,event)})},_deselectMarker(e,detail){// If singleInfoWindow is set, update iron-selector's selected attribute to be null.
// Else remove the marker from iron-selector's selected array.
const markerIndex=this.$.selector.indexOf(e.target);if(this.singleInfoWindow){this.$.selector.selected=null}else if(this.$.selector.selectedValues){this.$.selector.selectedValues=this.$.selector.selectedValues.filter(i=>i!==markerIndex)}}});/**
     * @license
     * Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
     * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
     * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
     * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
     * Code distributed by Google as part of the polymer project is also
     * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
     */(function(){'use strict';/**
                 * Basic flow of the loader process
                 *
                 * There are 4 flows the loader can take when booting up
                 *
                 * - Synchronous script, no polyfills needed
                 *   - wait for `DOMContentLoaded`
                 *   - fire WCR event, as there could not be any callbacks passed to `waitFor`
                 *
                 * - Synchronous script, polyfills needed
                 *   - document.write the polyfill bundle
                 *   - wait on the `load` event of the bundle to batch Custom Element upgrades
                 *   - wait for `DOMContentLoaded`
                 *   - run callbacks passed to `waitFor`
                 *   - fire WCR event
                 *
                 * - Asynchronous script, no polyfills needed
                 *   - wait for `DOMContentLoaded`
                 *   - run callbacks passed to `waitFor`
                 *   - fire WCR event
                 *
                 * - Asynchronous script, polyfills needed
                 *   - Append the polyfill bundle script
                 *   - wait for `load` event of the bundle
                 *   - batch Custom Element Upgrades
                 *   - run callbacks pass to `waitFor`
                 *   - fire WCR event
                 */var polyfillsLoaded=!1,whenLoadedFns=[],allowUpgrades=!1,flushFn;function fireEvent(){window.WebComponents.ready=!0;document.dispatchEvent(new CustomEvent("WebComponentsReady",{bubbles:!0}))}function batchCustomElements(){if(window.customElements&&customElements.polyfillWrapFlushCallback){customElements.polyfillWrapFlushCallback(function(flushCallback){flushFn=flushCallback;if(allowUpgrades){flushFn()}})}}function asyncReady(){batchCustomElements();ready()}function ready(){// bootstrap <template> elements before custom elements
if(window.HTMLTemplateElement&&HTMLTemplateElement.bootstrap){HTMLTemplateElement.bootstrap(window.document)}polyfillsLoaded=!0;runWhenLoadedFns().then(fireEvent)}function runWhenLoadedFns(){allowUpgrades=!1;var fnsMap=whenLoadedFns.map(function(fn){return fn instanceof Function?fn():fn});whenLoadedFns=[];return Promise.all(fnsMap).then(function(){allowUpgrades=!0;flushFn&&flushFn()}).catch(function(err){console.error(err)})}window.WebComponents=window.WebComponents||{};window.WebComponents.ready=window.WebComponents.ready||!1;window.WebComponents.waitFor=window.WebComponents.waitFor||function(waitFn){if(!waitFn){return}whenLoadedFns.push(waitFn);if(polyfillsLoaded){runWhenLoadedFns()}};window.WebComponents._batchCustomElements=batchCustomElements;var name="webcomponents-loader.js",polyfills=[];// Feature detect which polyfill needs to be imported.
if(!("attachShadow"in Element.prototype&&"getRootNode"in Element.prototype)||window.ShadyDOM&&window.ShadyDOM.force){polyfills.push("sd")}if(!window.customElements||window.customElements.forcePolyfill){polyfills.push("ce")}var needsTemplate=function(){// no real <template> because no `content` property (IE and older browsers)
var t=document.createElement("template");if(!("content"in t)){return!0}// broken doc fragment (older Edge)
if(!(t.content.cloneNode()instanceof DocumentFragment)){return!0}// broken <template> cloning (Edge up to at least version 17)
var t2=document.createElement("template");t2.content.appendChild(document.createElement("div"));t.content.appendChild(t2);var clone=t.cloneNode(!0);return 0===clone.content.childNodes.length||0===clone.content.firstChild.content.childNodes.length}();// NOTE: any browser that does not have template or ES6 features
// must load the full suite of polyfills.
if(!window.Promise||!Array.from||!window.URL||!window.Symbol||needsTemplate){polyfills=["sd-ce-pf"]}if(polyfills.length){var url,polyfillFile="bundles/webcomponents-"+polyfills.join("-")+".js";// Load it from the right place.
if(window.WebComponents.root){url=window.WebComponents.root+polyfillFile}else{var script=document.querySelector("script[src*=\""+name+"\"]");// Load it from the right place.
url=script.src.replace(name,polyfillFile)}var newScript=document.createElement("script");newScript.src=url;// if readyState is 'loading', this script is synchronous
if("loading"===document.readyState){// make sure custom elements are batched whenever parser gets to the injected script
newScript.setAttribute("onload","window.WebComponents._batchCustomElements()");document.write(newScript.outerHTML);document.addEventListener("DOMContentLoaded",ready)}else{newScript.addEventListener("load",function(){asyncReady()});newScript.addEventListener("error",function(){throw new Error("Could not load polyfill bundle"+url)});document.head.appendChild(newScript)}}else{// if readyState is 'complete', script is loaded imperatively on a spec-compliant browser, so just fire WCR
if("complete"===document.readyState){polyfillsLoaded=!0;fireEvent()}else{// this script may come between DCL and load, so listen for both, and cancel load listener if DCL fires
window.addEventListener("load",ready);window.addEventListener("DOMContentLoaded",function(){window.removeEventListener("load",ready);ready()})}}})();class AppAtms extends _myAppnav.PolymerElement{static get properties(){return{api:{type:String,value:"AIzaSyAxNVayhHgcFDCnRBsv33nmh-qdja5iEIw"},token:{type:String,value:""},branches_atms:{type:Array},isAtms:{type:Boolean,value:!1}}}static get template(){return _myAppnav.html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        .card{
          text-align:center;
          background-color: #B3E5FC;
        }
        google-map {
        position:relative !important;
        left:20%;
        height: 400px;
        width: 600px;
        border-radius: 10px;
        }    
      </style>
      <my-app>
      <div class="card">
      <h5 class="card-header">Mis Cajeros y Sucursales.</h5>
        <div class="card-body">
            <div class="row">
              <div class="col-md-12">
              <div class="btn-group btn-group-toggle" style="margin-bottom:10px;" data-toggle="buttons">
                  <label id="option1" class="btn btn-primary active">
                    <input type="radio" name="options" on-click="onclickOption1" autocomplete="off"><iron-icon icon="account-balance"></iron-icon>Sucursales
                  </label>
                  <label id="option2" class="btn btn-outline-primary">
                    <input type="radio" name="options" on-click="onclickOption2" autocomplete="off"><iron-icon icon="maps:local-atm"></iron-icon>Cajeros
                  </label>
                </div>
              </div>   
            </div>
            <div class="row">
              <div class="col col-xs-12 col-md-6 col-lg-12" style="text-align:center;">
              <google-map fit-to-markers latitude="19.432387" longitude="-99.132713" api-key="[[api]]" zoom="15" disable-default-ui>
                <template is=dom-repeat items="{{branches_atms}}">
                  <google-map-marker latitude="[[item.address.geolocation.latitude]]" longitude="[[item.address.geolocation.longitude]]" title="Go Giants!" draggable="false">
                  <div class="card" style="width: 12rem;">
                    <template is="dom-if" if="[[!isAtms]]" restamp>
                      <img class="card-img-top" src="../../images/branch.jpeg" width="100px" alt="Card image cap">
                    </template>
                    <template is="dom-if" if="[[isAtms]]" restamp>
                      <img class="card-img-top" src="../../images/atm.jpg" width="100px" alt="Card image cap">
                    </template>
                    <div class="card-body">
                      <label>{{item.address.country.name}} {{item.address.state.name}}</label>
                       <p>{{item.description}} {{item.address.city}}</p>
                       <p>{{item.address.addressName}}</p>
                    </div>
                  </div>  
                  </google-map-marker>
                </template>
              </google-map>
              </div>   
            </div>
        </div>
    <iron-ajax
          id="getAtmsOrBranches"
          content-type="application/json"
          crossDomain: true
          method="GET",
          handle-as="json"
          on-response="manageAJAXResponse"
          on-error="showError"></iron-ajax>
      </div>
      </my-app>
    `}ready(){super.ready();console.log("page atms");this.checkToken();this.initMap()}checkToken(){let mytoken=sessionStorage.getItem("Token");if(!isEmpty(mytoken)){this.token="Bearer "+mytoken}}initMap(){let myemail=sessionStorage.getItem("Email");if(this.isAtms){this.$.getAtmsOrBranches.url=baseApi+"atms"}else{this.$.getAtmsOrBranches.url=baseApi+"branches"}this.$.getAtmsOrBranches.headers={"Access-Control-Allow-Origin":"*",Authorization:this.token,customer:myemail};this.$.getAtmsOrBranches.generateRequest()}onclickOption1(){this.isAtms=!1;this.$.option1.className="btn btn-primary active";this.$.option2.className="btn btn-outline-primary";this.initMap()}onclickOption2(){this.isAtms=!0;this.$.option2.className="btn btn-primary active";this.$.option1.className="btn btn-outline-primary";this.initMap()}manageAJAXResponse(e,request){this.branches_atms=request.response.data}showError(e,error){Toast.fire({icon:"error",title:"Error al consultar los Atms y sucursales."})}}window.customElements.define("app-atms",AppAtms)});