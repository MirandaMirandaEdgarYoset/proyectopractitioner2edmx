define(["../my-appnav.js","./my-app.js"],function(_myAppnav,_myApp){"use strict";class Movements extends _myAppnav.PolymerElement{static get properties(){return{inMovements:{type:Object},outMovements:{type:Object}}}static get template(){return _myAppnav.html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
       <div class="card">
            <h5 class="card-header">Tus Movimientos.</h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-12">
                        <table class="table table-hover table-sm table-responsive-sm">
                            <thead>
                                <tr class="bg-info">
                                    <th colspan="4">Movimientos entrantes </th>
                                </tr>
                                <tr>
                                    <th>Cuenta de envío</th>
                                    <th>Cuenta receptora</th>
                                    <th>Monto</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                <template is="dom-repeat" items="[[inMovements]]">
                                <tr>
                                    <td>{{format(item.sendAccount)}}</td> 
                                    <td>{{format(item.receiveAccount)}}</td>
                                    <td>$ {{item.amount.$numberDecimal}}</td>
                                    <td>{{item.date}}</td>
                                </tr>
                                </template>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-12">
                        <table class="table table-hover table-sm table-responsive-sm">
                            <thead>
                                <tr class="bg-info">
                                    <th colspan="4">Movimientos salientes </th>
                                </tr>
                                <tr>
                                    <th>Cuenta de envío</th>
                                    <th>Cuenta receptora</th>
                                    <th>Monto</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                <template is="dom-repeat" items="[[outMovements]]">
                                <tr>
                                    <td>{{format(item.sendAccount)}}</td> 
                                    <td>{{format(item.receiveAccount)}}</td>
                                    <td>$ {{item.amount.$numberDecimal}}</td>
                                    <td>{{item.date}}</td>
                                </tr>
                                </template>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <iron-ajax
            id=getMovements
            content-type="application/json"
            crossDomain: true
            method="GET"
            handle-as="json"
            headers="Access-Control-Allow-Headers: *"
            on-response="manageAJAXResponse"
            on-error="showError">
        </iron-ajax>
        `}ready(){super.ready();let myemail=sessionStorage.getItem("Email"),mytoken=sessionStorage.getItem("Token");this.$.getMovements.url=baseApi+"movements";this.$.getMovements.headers={"Access-Control-Allow-Origin":"*",Authorization:"Bearer "+mytoken,customer:myemail};this.$.getMovements.generateRequest()}manageAJAXResponse(e,request){let data=request.response,status=data.status,movimientos=request.response.movements;if(movimientos){this.processMovementsData(movimientos)}switch(status){case 200:Toast.fire({icon:"success",title:"Movimientos obtenidos"});//this.dispatchEvent(new CustomEvent('logguedIn',{detail: {response:request.response}}));
break;}}processMovementsData(movimientos){const mov=movimientos;// const singers = [
//     { name: 'Steven Tyler', band: 'Aerosmith', born: 1948 },
//     { name: 'Karen Carpenter', band: 'The Carpenters', born: 1950 },
//     { name: 'Kurt Cobain', band: 'Nirvana', born: 1967 },
//     { name: 'Stevie Nicks', band: 'Fleetwood Mac', born: 1948 },
// ];
function compareValues(key,order="asc"){return function innerSort(a,b){if(!a.hasOwnProperty(key)||!b.hasOwnProperty(key)){// property doesn't exist on either object
return 0}const varA="string"===typeof a[key]?a[key].toUpperCase():a[key],varB="string"===typeof b[key]?b[key].toUpperCase():b[key];let comparison=0;if(varA>varB){comparison=1}else if(varA<varB){comparison=-1}return"desc"===order?-1*comparison:comparison}}let customer=sessionStorage.getItem("customerId"),outMovements=mov.filter(function(el){return el.customerId===customer});this.outMovements=outMovements;let inMovements=mov.filter(function(el){return el.customerId!=customer});console.log("el array filtrado por movimientos de entrada: ",inMovements);this.inMovements=inMovements;let arrayOrdenado=mov.sort(compareValues("customerId","desc"));this.data=mov}showError(e,error){let errorResponse=e.detail.request.xhr.response;//Toast.fire({icon: 'error',title: errorResponse});
//this.dispatchEvent(new CustomEvent('errorLogin',{detail: {request:error.request.status}}));
}format(tarjeta){if(tarjeta==void 0||null==tarjeta)return"";let str=tarjeta.toString(),res=str.match(/.{1,4}/g);return res.join("-")}}window.customElements.define("practitioner-movements",Movements);class AppMovements extends _myAppnav.PolymerElement{static get template(){return _myAppnav.html`

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style include="shared-styles">
      :host {
        display: block;

        padding: 10px;
      }
      .card{
        background-color: #B3E5FC;
        color: #000000;
      }
    </style>
    <my-app>
    <div class="card" class="col-xs-12 col-md-6 col-lg-12">
      <practitioner-movements></practitioner-movements>
    </div>
    </my-app>
    `}}window.customElements.define("app-movements",AppMovements)});