define(["../my-appnav.js"],function(_myAppnav){"use strict";class AppError404 extends _myAppnav.PolymerElement{static get template(){return _myAppnav.html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <style>
        :host {
          display: block;

          padding: 10px 20px;
        }
        .error{
          text-align:center;
          position:relative;
          left:10%;
          top:20%;
        }
        .error h1{
          font-size:150px;
        }
        .error h2{
          font-size:90px;
        }
      </style>
      <my-app>
      <div class="error">
          <div class="card border-warning mb-3" style="max-width: 54rem;">
          <div class="card-body text-warning">
              <h1 class="card-title">Oops!</h1>
              <h2 class="card-title">404!</h2>
              <p class="card-text">No encontramos el recurso que buscas.</p>
              <a href="[[rootPath]]dashboard" class="badge badge-warning">Regresar</a>
            </div>
          </div>
      </my-app>
    `}alerta(){console.log("alerta");Swal.fire("Any fool can use a computer")}}window.customElements.define("app-error404",AppError404)});