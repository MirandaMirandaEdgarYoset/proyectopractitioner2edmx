define(["../my-appnav.js"],function(_myAppnav){"use strict";class Login extends _myAppnav.PolymerElement{static get properties(){return{userName:{type:String},password:{type:String}}}static get template(){return _myAppnav.html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <style>
        .login-box {
        width: 400px;
        height: 500px;
        background: #000;
        color: #fff;
        top: 50%;
        left: 50%;
        box-sizing: border-box;
        padding: 70px 30px;
        border-radius: 10px;
        position: absolute;
        transform: translate(-50%,20%);
        }

        .login-box .avatar {
        width: 80px;
        height: 80px;
        border-radius: 50%;
        position: relative;
        top: 50%;
        left:50%;
        transform: translate(-50%,5%);
        text-align:center;
        }

        .login-box h1 {
        margin: 0;
        padding: 0 0 20px;
        text-align: center;
        font-size: 22px;
        }

        .login-box label {
        margin: 0;
        padding: 0;
        font-weight: bold;
        display: block;
        }

        .login-box input {
        width: 100%;
        margin-bottom: 20px;
        }

        .login-box input[type="text"], .login-box input[type="password"] {
        border: none;
        border-bottom: 1px solid #fff;
        background: transparent;
        outline: none;
        height: 40px;
        color: #fff;
        font-size: 16px;
        }

        .login-box input[type="submit"] {
        border: none;
        outline: none;
        height: 40px;
        background: #b80f22;
        color: #fff;
        font-size: 18px;
        border-radius: 20px;
        }

        .login-box input[type="submit"]:hover {
        cursor: pointer;
        background: #ffc107;
        color: #000;
        }

        .login-box a {
        text-decoration: none;
        font-size: 12px;
        line-height: 20px;
        color: darkgrey;
        }

        .login-box a:hover {
        color: #fff;
        }
        </style>
        <div class="login-box">
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/1024px-User_icon_2.svg.png" class="avatar" alt="Avatar Image">
        <h1>Iniciar sesión</h1>
        <div>
          <!-- USERNAME INPUT -->
          <label for="username">Usuario</label>
          <input type="text" placeholder="Ingresar usuario" value="{{userName::input}}" required>
          <!-- PASSWORD INPUT -->
          <label for="password">Contraseña</label>
          <input type="password" placeholder="Ingresar contraseña" value="{{password::input}}" required>
          <button class="btn btn-warning btn-block" on-click="login">Login</button>
          
          </div>
        <iron-ajax
          id=doLogin
          content-type="application/json"
          crossDomain: true
          method="POST"
          handle-as="json"
          headers="Access-Control-Allow-Headers: *"
          on-response="manageAJAXResponse"
          on-error="showError"></iron-ajax>
      </div>
        `}ready(){super.ready();/* this.$.formLogin.addEventListener("submit",e=>{
                     e.preventDefault();
                     this.login();
                   }); */}login(){let loginData={email:this.userName,password:this.password};//console.log(loginData);
this.$.doLogin.url=baseApi+"login";this.$.doLogin.body=JSON.stringify(loginData);if(this.isValidForm()){this.$.doLogin.generateRequest();this.cleanForm()}}manageAJAXResponse(e,request){let data=request.response,status=data.status;switch(status){case 200:Toast.fire({icon:"success",title:"Sesion iniciada"});this.saveTempUser(data.user.customerId,data.token,data.user.user,data.user.email);this.dispatchEvent(new CustomEvent("logguedIn",{detail:{response:request.response}}));break;}}showError(e,error){this.cleanForm();Toast.fire({icon:"error",title:"Usuario y contrase\xF1a Incorrectos"});this.dispatchEvent(new CustomEvent("errorLogin",{detail:{request:error.request}}))}cleanForm(){this.userName="";this.password=""}isValidForm(){if(isEmpty(this.userName)||isEmpty(this.password)){Toast.fire({icon:"info",title:"Completa los campos"});return!1}return!0}saveTempUser(customerId,token,userName,email){sessionStorage.setItem("customerId",customerId);sessionStorage.setItem("Token",token);sessionStorage.setItem("UserName",userName);sessionStorage.setItem("Email",email)}}window.customElements.define("practitioner-login",Login);class AppLogin extends _myAppnav.PolymerElement{static get template(){return _myAppnav.html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
    :host {
      --app-primary-color: #4285f4;
      --app-secondary-color: black;

      
    }

    app-drawer-layout:not([narrow]) [drawer-toggle] {
      display: none;
    }

    app-header {
      color: #fff;
      background-color: #061053;
    }

    app-header paper-icon-button {
      --paper-icon-button-ink-color: white;
    }

    .drawer-list {
      margin: 0 20px;
    }

    .drawer-list a {
      display: block;
      padding: 0 16px;
      text-decoration: none;
      color: var(--app-secondary-color);
      line-height: 40px;
    }
  </style>  
        <app-location route="{{route}}"></app-location>
        <app-route
            route="{{route}}"
            pattern="/:view"
            data="{{routeData}}"
            tail="{{subroute}}"></app-route>          
        <div id="principal" class="row">
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar>
              <div main-title="" style="text-align:center;">Mi Banca Online</div>
            </app-toolbar>
          </app-header>
        </app-header-layout>
      </app-drawer-layout>
      <div class="col-md-12">
          <practitioner-login id="loginComponent" titulo="Log in" subtitulo="Bienvenido" boton="Log in"></practitioner-login>
      </div>
    `}ready(){super.ready();this.$.loginComponent.addEventListener("logguedIn",e=>{this.dispatchEvent(new CustomEvent("logguedInApp",{detail:{response:e.detail.response}}));this.set("route.path","dashboard")});this.$.loginComponent.addEventListener("errorLogin",e=>{this.dispatchEvent(new CustomEvent("errorLoginApp",{detail:{request:e.detail.request}}))});this.checkIsLoggueIn()}checkIsLoggueIn(){let customerId=sessionStorage.getItem("customerId"),token=sessionStorage.getItem("Token"),username=sessionStorage.getItem("UserName"),email=sessionStorage.getItem("Email");if(!isEmpty(customerId)&&!isEmpty(token)&&!isEmpty(email)&&!isEmpty(username)){this.set("route.path","dashboard")}}}window.customElements.define("app-login",AppLogin)});