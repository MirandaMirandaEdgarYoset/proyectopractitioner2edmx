define(["../my-appnav.js"],function(_myAppnav){"use strict";class Register extends _myAppnav.PolymerElement{static get properties(){return{nombre:{type:String},apellidos:{type:String},email:{type:String},password:{type:String}}}static get template(){return _myAppnav.html`
        <style>
        body {
          font-family: Arial, Helvetica, sans-serif;
          background-color: black;
        }
        
        * {
          box-sizing: border-box;
        }
        
        /* Add padding to containers */
        .container {
          padding: 16px;
          background-color: white;
        }
        
        /* Full-width input fields */
        input[type=text], input[type=password], input[type=email] {
          width: 100%;
          padding: 15px;
          margin: 5px 0 22px 0;
          display: inline-block;
          border: none;
          background: #f1f1f1;
        }
        
        input[type=text]:focus, input[type=password]:focus {
          background-color: #ddd;
          outline: none;
        }
        
        /* Overwrite default styles of hr */
        hr {
          border: 1px solid #f1f1f1;
          margin-bottom: 25px;
        }
        
        /* Set a style for the submit button */
        .registerbtn {
          background-color: #4CAF50;
          color: white;
          padding: 16px 20px;
          margin: 8px 0;
          border: none;
          cursor: pointer;
          width: 100%;
          opacity: 0.9;
        }
        
        .registerbtn:hover {
          opacity: 1;
        }
        
        /* Add a blue text color to links */
        a {
          color: dodgerblue;
        }
        
        /* Set a grey background color and center the text of the "sign in" section */
        .signin {
          background-color: #f1f1f1;
          text-align: center;
        }
        </style>

        <form id="formRegister">
            <div class="container">
            <h1>Registro</h1>
            <p>Ingresa tus datos para crear tu usuario.</p>
            <hr>

            <label for="nombre"><b>Nombre</b></label>
            <input type="text" placeholder="Nombre" name="nombre" id="nombre" value="{{nombre::input}}" required>

            <label for="apellidos"><b>Apellidos</b></label>
            <input type="text" placeholder="Apellidos" name="email" id="email" value="{{apellidos::input}}" required>

            <label for="email"><b>Email</b></label>
            <input type="email" placeholder="Email" name="email" id="email" value="{{email::input}}" required>

            <label for="psw"><b>Password</b></label>
            <input type="password" placeholder="Password" name="psw" id="psw" value="{{password::input}}" required>

            <hr>
            <p>Al dar de alta tu usuario aplica nuestros <a href="#">Terminos y condiciones</a>.</p>

            <button type="submit" class="registerbtn" >Registrar</button>
            </div>

            <div class="container signin">
            <p>¿Ya tienes un usuario? <a href="#">Sign in</a>.</p>
            </div>
        </form>

        <iron-ajax
        id=doRegister
        content-type="application/json"
        crossDomain: true
        method="POST"
        handle-as="json"
        headers="Access-Control-Allow-Headers: *"
        on-response="manageAJAXResponse"
        on-error="showError"></iron-ajax>
        `}ready(){super.ready();this.$.doLogin.url=baseApi+"register";this.$.formRegister.addEventListener("submit",e=>{this.register();e.preventDefault()})}register(){let registerData={userName:this.nombre,lastName:this.apellidos,email:this.email,password:this.password};this.$.doRegister.body=JSON.stringify(registerData);this.$.doRegister.generateRequest()}manageAJAXResponse(e,request){let data=request.response,status=data.status;switch(status){case 200:Toast.fire({icon:"success",title:"Usuario registrado correctamente"});setTimeout(()=>{location.replace("http://127.0.0.1:8081/")},3e3);//this.dispatchEvent(new CustomEvent('logguedIn',{detail: {response:request.response}}));
break;}}showError(e,error){let errorResponse=e.detail.request.xhr.response.error;Toast.fire({icon:"error",title:errorResponse});//this.dispatchEvent(new CustomEvent('errorLogin',{detail: {request:error.request.status}}));
}}window.customElements.define("practitioner-register",Register);class AppRegister extends _myAppnav.PolymerElement{static get template(){return _myAppnav.html`
    <practitioner-register></practitioner-register>
    `}}window.customElements.define("app-register",AppRegister)});